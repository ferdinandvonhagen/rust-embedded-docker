FROM rust:1.63.0-buster

RUN apt-get update && \
    apt-get install -y apt-transport-https ca-certificates gnupg software-properties-common wget && \
    wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add - && \
    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' && \
    apt-get remove -y apt-transport-https ca-certificates gnupg software-properties-common && \
    apt-get clean all && \
    apt-get update && \
    apt-get install -y cmake gcc-arm-none-eabi libclang-dev python-pip && \
    pip install pre-commit && \
    rustup target add thumbv7em-none-eabihf && \
    cargo install --force cbindgen && \
    rustup component add rustfmt && \
    rustup component add clippy && \
    apt-get remove -y python-pip apt-transport-https ca-certificates gnupg wget && \
    apt-get clean all && \
    rm -rf /var/lib/apt/lists/*

